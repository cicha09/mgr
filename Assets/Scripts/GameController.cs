﻿using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    [Header("References")]
    public BoardController boardController;
    public ActionsController actionsController;
    public HUDController hudController;
    public RobotController robot;
    public TutorialController tutorialController;
    public CameraController gameCamera;
    public MainMenuController mainMenu;
    public AudioController gameAudio;
    public LocalizationManager localization;
    public TypingController typing;
    public FunctionsMatcher functionsMatcher;

    public GameSet gameSet = null;

    private GameObject currentContent = null;

    private void Awake()
    {
        boardController = GetComponent<BoardController>();
        actionsController = GetComponent<ActionsController>();
        hudController = GetComponent<HUDController>();
        robot = FindObjectOfType<RobotController>();
        tutorialController = FindObjectOfType<TutorialController>();
        gameCamera = Camera.main.GetComponent<CameraController>();
        mainMenu = FindObjectOfType<MainMenuController>();
        gameAudio = FindObjectOfType<AudioController>();
        localization = FindObjectOfType<LocalizationManager>();
        typing = FindObjectOfType<TypingController>();
        functionsMatcher = GetComponent<FunctionsMatcher>();
    }

    public void SetGame(LevelSettings levelSettings)
    {
        if (currentContent != null)
            gameSet.RemoveContent(); // Debug.Log("Kontent już istnieje");

        gameSet = new GameSet();
        gameSet.levelSettings = levelSettings;

        currentContent = gameSet.levelSettings.contentPrefab;

        PrepareRobot(levelSettings.robotPosition, levelSettings.robotRotation);
        PrepareScene(levelSettings);
        PrepareGamePanel(levelSettings);

        if (levelSettings.typingTextKeys.Count > 0)
            typing.ui.EnablePanel(levelSettings.typingTextKeys, AfterTypingAction);
        else
            AfterTypingAction();
    }

    public void SetGameRetry(LevelSettings levelSettings)
    {
        gameSet = new GameSet();
        gameSet.levelSettings = levelSettings;

        PrepareRobot(levelSettings.robotPosition, levelSettings.robotRotation);
        //PrepareScene(levelSettings);
        PrepareGamePanel(levelSettings);
        //if (levelSettings.typingTextKeys.Count > 0)
        //{
        //    typing.ui.EnablePanel(levelSettings.typingTextKeys, AfterTypingAction);
        //}
        //else
        //{
        //    AfterTypingAction();
        //}
    }

    public void EndEditing()
    {
        gameSet.StopTimeCounter();
        hudController.gameUI.LockGamePanel();
    }

    private void AfterTypingAction()
    {
        if (!gameSet.levelSettings.tutorial)
        {
            hudController.gameUI.methodsUI.PreapareMethodList(gameSet.levelSettings.methods, false);
            hudController.gameUI.SetGamePanel();
            gameSet.StartCounter(hudController, this);
        }
        else
        {
            if (gameSet.levelSettings.id == 0)
                hudController.gameUI.SetTutorialGamePanel();
        }
    }

    public void RetryGame()
    {
        if (!gameSet.levelSettings.tutorial)
        {
            gameSet.RemoveContent();
            SetGame(gameSet.levelSettings);
        }
        else
        {
        }
    }

    public void EndGame(bool gameOver)
    {
        if (!gameOver)
            gameSet.levelSettings.isComplited = true;

        gameSet.CheckGameOver(gameOver);
        hudController.gameUI.UnsetGamePanel();
        hudController.endingPanel.SetEndGame(gameSet.GetGameStats());
    }

    public void AddMoveToGameStat()
    {
        gameSet.AddMove();
    }

    #region PreparingScene
    private void PrepareScene(LevelSettings levelSettings)
    {
        GameObject content = Instantiate(levelSettings.contentPrefab, levelSettings.instantiateContentPosition, Quaternion.identity) as GameObject;
        gameSet.content = content;

        GameObject destinationTile = content.transform.Find("destination_tile").gameObject;
        boardController.PrepareBoard(destinationTile, content);

        Transform cameraTransform = content.transform.Find("camera_transform").transform;
        gameCamera.movement.Displace(cameraTransform.position, cameraTransform.rotation);

        Transform alarm = content.transform.Find("alarm");
        if (alarm != null)
        {
            if (alarm.GetComponent<AlarmDetector>())
                gameSet.alarmDetector = alarm.GetComponent<AlarmDetector>();
        }

        if (content.transform.Find("Light"))
            gameSet.levelLight = content.transform.Find("Light").GetComponent<Light>();
        if (content.transform.Find("detected"))
            gameSet.alarm = content.transform.Find("detected").GetComponent<Animator>();
    }

    private void PrepareRobot(Vector3 robotPosition, Vector3 robotRotation)
    {
        robot.SetRobotPositionAndRotation(robotPosition, robotRotation);
    }

    private void PrepareGamePanel(LevelSettings levelSettings)
    {
        hudController.gameUI.PreparePanel(levelSettings);
    }
    #endregion

    public void TimeOut()
    {
        EndGame(true);

        if (gameSet.alarm != null)
            gameSet.alarm.enabled = true;

        if (gameSet.levelLight != null)
            gameSet.levelLight.gameObject.SetActive(false);

        if (gameSet.alarm != null)
            gameSet.alarmDetector.gameObject.SetActive(false);
    }
}

public class GameSet : MonoBehaviour
{
    private GameStats gameStats = new GameStats();
    private TimeCounter timeCounter;

    public GameObject content = null;
    public LevelSettings levelSettings;
    public AlarmDetector alarmDetector;
    public Light levelLight;
    public Animator alarm;

    public void StartCounter(HUDController hudController, GameController gameController)
    {
        if (hudController.gameUI.timerField != null)
        {
            if (levelSettings.timer != 0)
            {
                GameObject counterObject = new GameObject();
                counterObject.name = "Time counter";
                timeCounter = counterObject.AddComponent<TimeCounter>();
                timeCounter.StartCount(hudController.gameUI.timerField, levelSettings.timer, gameController);

                if (alarmDetector != null)
                    alarmDetector.StartAlarm(levelSettings.timer);
            }
            else
                hudController.gameUI.PauseTime();
        }
        else
            Debug.LogWarning("The counter wasn't created.");
    }

    public void AddMove()
    {
        gameStats.Moves++;
    }

    public void StopTimeCounter()
    {
        if (timeCounter != null)
            gameStats.Time = timeCounter.StopCountAndReturnTime();
    }

    public void CheckGameOver(bool gameOver)
    {
        gameStats.IsGameOver = gameOver;
    }

    public GameStats GetGameStats()
    {
        return gameStats;
    }

    public void RemoveContent()
    {
        if (content != null)
            Destroy(content);
    }

    public void IsComplited(bool isComplited)
    {
        levelSettings.isComplited = isComplited;
    }
}