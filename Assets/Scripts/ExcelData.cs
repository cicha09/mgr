﻿using System.Collections.Generic;
using UnityEngine;

public class ExcelData
{
    private string dataPath = "";
    private string dataBackupsPath = "";

    public ExcelData()
    {
        dataPath = Application.dataPath + "/Resources/Data/Strings.xlsx";
        dataBackupsPath = Application.dataPath + "/Resources/Data/Backups/";
    }

    public List<ResourceEntry> Read()
    {
        List<ResourceEntry> resourceEntries = new List<ResourceEntry>();
        Excel xls = ExcelHelper.LoadExcel(dataPath);

        int numberOfRows = xls.Tables[0].NumberOfRows;

        for (int i = 2; i <= numberOfRows; i++)
        {
            int string_id = int.Parse(xls.Tables[0].GetValue(i, 1).ToString());
            string string_EN = xls.Tables[0].GetValue(i, 2).ToString();
            string string_PL = xls.Tables[0].GetValue(i, 3).ToString();

            ResourceEntry entry = new ResourceEntry(string_id, string_EN, string_PL);
            resourceEntries.Add(entry);
        }

        Debug.Log("Data successfully READ from file: " + dataPath);
        return resourceEntries;
    }

    public void Write(List<ResourceEntry> resourceEntries)
    {
        if (ExcelHelper.LoadExcel(dataPath) == null)
            ExcelHelper.CreateExcel(dataPath);

        ExcelHelper.SaveExcel(GetXls(resourceEntries, dataPath), dataPath);
        Debug.Log("Data successfully WRITTEN to file:: " + dataPath);
    }

    private Excel GetXls(List<ResourceEntry> resourceEntries, string dataPath)
    {
        Excel xls = new Excel();
        ExcelTable table = new ExcelTable();
        table.TableName = "Strings";
        xls.Tables.Add(table);

        int numberOfRows = resourceEntries.Count + 1;
        int numberOfColumns = 3;
        int listCounter = 0;

        xls.Tables[0].SetValue(1, 1, "ID");
        xls.Tables[0].SetValue(1, 2, "English");
        xls.Tables[0].SetValue(1, 3, "Polish");

        for (int i = 2; i <= numberOfRows; i++)
        {
            for (int j = 1; j <= numberOfColumns; j++)
            {
                if (j == 1) xls.Tables[0].SetValue(i, j, resourceEntries[listCounter].string_id.ToString());
                else if (j == 2) xls.Tables[0].SetValue(i, j, resourceEntries[listCounter].string_EN.ToString());
                else if (j == 3) xls.Tables[0].SetValue(i, j, resourceEntries[listCounter].string_PL.ToString());
            }
            listCounter++;
        }

        return xls;
    }

    public void MakeBackup(List<ResourceEntry> resourceEntries)
    {
        string filePath = dataBackupsPath + "Strings_"
            + System.DateTime.Now.Day.ToString() + "_"
            + System.DateTime.Now.Month.ToString() + "_time_"
            + System.DateTime.Now.Hour.ToString() + "_"
            + System.DateTime.Now.Minute.ToString() + "_"
            + System.DateTime.Now.Second.ToString() + ".xlsx";

        if (ExcelHelper.LoadExcel(filePath) == null)
            ExcelHelper.CreateExcel(filePath);

        ExcelHelper.SaveExcel(GetXls(resourceEntries, filePath), filePath);
        Debug.Log("Backup successfully created: " + dataPath);
    }
}