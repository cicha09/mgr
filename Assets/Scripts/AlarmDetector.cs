﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlarmDetector : MonoBehaviour
{
    public Vector3 targetRotation;

    private Vector3 startRotation;
    private bool transforming = false;
    private float t = 0;

    public void StartAlarm(float time)
    {
        StartCoroutine(MoveOverSeconds(gameObject, targetRotation, time));
    }

    public IEnumerator MoveOverSeconds(GameObject objectToMove, Vector3 end, float seconds)
    {
        float elapsedTime = 0;
        Vector3 startingPos = objectToMove.transform.position;
        while (elapsedTime < seconds)
        {
            objectToMove.transform.position = Vector3.Lerp(startingPos, end, (elapsedTime / seconds));
            elapsedTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        objectToMove.transform.position = end;
    }
}