﻿using System;
using UnityEngine;

public class AudioController : MonoBehaviour
{
    public Audios audios;

    public AudioSource uiAudiosSource;
    public AudioSource uiButtonsSource;

    public enum MouseEvents { enter, exit, down };
    private MouseEvents mouseEvents;

    private void SetAudio(AudioSource audioSource, AudioClip clip, bool loop)
    {
        if (audioSource != null && clip != null)
        {
            audioSource.loop = loop;
            if (loop)
            {
                audioSource.clip = clip;
                audioSource.Play();
            }
            else
                audioSource.PlayOneShot(clip);
        }
    }

    #region UI Audio
    public void StopUIAudio()
    {
        if (uiAudiosSource != null)
            uiAudiosSource.Stop();
    }

    public void SetMouseEvent(MouseEvents type)
    {
        if (type == MouseEvents.enter)
            SetAudio(uiButtonsSource, audios.mouseEnter, false);
        else if (type == MouseEvents.exit)
            SetAudio(uiButtonsSource, audios.mouseExit, false);
        else if (type == MouseEvents.down)
            SetAudio(uiButtonsSource, audios.mouseDown, false);
    }

    public void SetTyping() { SetAudio(uiAudiosSource,audios.keyboardTyping, true); }
    #endregion
}