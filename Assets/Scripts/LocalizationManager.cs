﻿using UnityEngine;

public class LocalizationManager : MonoBehaviour
{
    public Strings strings;

    public string GetLocalizedValue(int key)
    {
        if (key != -1)
            return strings.GetValueByKey(key);
        else
            return "error";
    }
}