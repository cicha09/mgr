﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class TutorialVideo : MonoBehaviour
{
    public List<GameObject> editors = new List<GameObject>();

    [Header("References")]
    public VideoPlayer videoPlayer;

    private int previousID = -1;

    private void Start()
    {
        videoPlayer = FindObjectOfType<VideoPlayer>();
        videoPlayer.gameObject.SetActive(false);
    }

    public void PlayVideo(Tutorial tutorial)
    {
        if (previousID != -1)
            editors[previousID].SetActive(false);

        videoPlayer.clip = tutorial.videoClip;
        editors[tutorial.id].SetActive(true);
        previousID = tutorial.id;

        videoPlayer.gameObject.SetActive(true);
        videoPlayer.Play();
    }

    public void StopVideo()
    {
        videoPlayer.gameObject.SetActive(false);
        videoPlayer.Stop();
    }
}