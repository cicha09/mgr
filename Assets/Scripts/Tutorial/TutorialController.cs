﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

public class TutorialController : MonoBehaviour
{
    [Header("References")]
    public TutorialVideo video;
    public TutorialUI ui;

    [Header("Settings")]
    public List<Tutorial> tutorials = new List<Tutorial>();
    public Tutorial CurrentTutorial { get;  set; }

    public List<GameObject> panels = new List<GameObject>();
    public EventTrigger skipButton;
    public CanvasGroup canvasGroup;

    private GameController game;
    private int currentPanelId = 0;

    private void Start()
    {
        game = Managers.instance.game;
        video = GetComponent<TutorialVideo>();
        ui = GetComponent<TutorialUI>();

        FillButton();
    }

    public void Toggle(bool enable)
    {
        if (enable)
        {
            panels[0].GetComponent<Animator>().enabled = enable;
            game.hudController.gameUI.ToggleInteractable(false);
        }
        else
            game.hudController.gameUI.ToggleVisibility(false);

        canvasGroup.alpha = enable ? 1 : 0;
        canvasGroup.interactable = enable;
        canvasGroup.blocksRaycasts = enable;

        game.gameCamera.effects.ToggleGreyscale(!enable);
    }

    public void SetTutorialPanel(int panelID)
    {
        currentPanelId = 0;
        Toggle(false);
        game.typing.ui.EnableTutorialPanel(panelID);
        game.hudController.gameUI.methodsUI.PreapareMethodList(CurrentTutorial.levelSettings.methods, true);
    }

    private void SkipAction()
    {
        panels[currentPanelId].GetComponent<Animator>().enabled = false;
        panels[currentPanelId].GetComponent<CanvasGroup>().alpha = 0;

        currentPanelId++;

        if (currentPanelId >= panels.Count)
        {
            SetTutorialPanel(game.gameSet.levelSettings.id);
            game.hudController.gameUI.ToggleInteractable(true);
            return;
        }
        panels[currentPanelId].GetComponent<Animator>().enabled = true;
    }

    private void FillButton()
    {
        EventTrigger.Entry pointerDownEntry = new EventTrigger.Entry();
        pointerDownEntry.eventID = EventTriggerType.PointerDown;
        pointerDownEntry.callback.AddListener((data) => { SkipAction(); });

        skipButton.triggers.Add(pointerDownEntry);
    }

    public Tutorial GetTutorial(int id)
    {
        CurrentTutorial = tutorials.First(s => s.id == id);
        return CurrentTutorial;
    }
}