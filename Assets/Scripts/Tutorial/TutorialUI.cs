﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class TutorialUI : MonoBehaviour
{
    public GameController game;

    public CanvasGroup tutorialLevelsPanel;
    public Transform listParent;
    public Transform levelsPanelParent;

    private List<ListElement> elements = new List<ListElement>();
    private List<LevelController> levels = new List<LevelController>();
    private int currentElement = 0;
    private ListElement previousElement;

    private void Start()
    {
        game = Managers.instance.game;

        CreateList();
        GetLevelControllers();
    }

    private void GetLevelControllers()
    {
        foreach (Transform child in levelsPanelParent.transform)
            levels.Add(child.GetComponent<LevelController>());
    }

    private void CreateList()
    {
        int counter = 0;
        foreach (Transform child in listParent)
        {
            elements.Add(new ListElement(
                counter,
                child.gameObject,
                child.Find("Function").GetComponent<Text>(),
                this
                ));

            counter++;
        }
    }

    public void AddMethodToTutorialEditor(string key)
    {
        foreach (var element in elements)
        {
            if (!element.IsActive())
            {
                element.Set(game.functionsMatcher.GetCodeLineByKey(key));
                RefreshList();
                return;
            }
        }

        RefreshList();
        Debug.Log("Masz ograniczone pole!");
    }

    public void AddMethodToTutorialEditor(string key, string parametr)
    {
        foreach (var element in elements)
        {
            if (!element.IsActive())
            {
                string line = game.functionsMatcher.GetCodeLineByKey(key);
                int signIndex = line.IndexOf('X');

                string part1 = line.Substring(0, signIndex);
                string codeLine = part1 + parametr + line.Substring(signIndex + 1, line.Length - part1.Length - 1);

                element.Set(codeLine);
                RefreshList();
                return;
            }
        }

        RefreshList();
        Debug.Log("Masz ograniczone pole!");
    }

    public void Move(ListElement element, int index)
    {
        int indexE1 = elements.IndexOf(element);

        var e1 = elements[indexE1];
        var e2 = elements[indexE1 + index];

        elements.RemoveAt(indexE1);
        elements.Insert(indexE1, e2);

        elements.RemoveAt(indexE1 + index);
        elements.Insert(indexE1 + index, e1);

        RefreshList();
    }

    public void RefreshList()
    {
        int counter = 0;
        currentElement = 0;

        if (previousElement != null)
        {
            previousElement.ToggleFollower(false);
            previousElement = null;
        }

        ListElement lastElement = null;
        foreach (var e in elements)
        {
            if (e.obj.activeInHierarchy)
            {
                lastElement = e;
                counter++;
            }
        }

        if (counter == 1)
        {
            lastElement.ToggleButtons(false, false);
            return;
        }

        ToggleLine();

        lastElement = null;
        foreach (var e in elements)
        {
            if (e.obj.activeInHierarchy)
                lastElement = e;
        }

        if (lastElement != null && counter != 1)
            lastElement.ToggleButtons(false, true);
    }

    private void ToggleLine()
    {
        bool first = true;
        for (int i = 0; i < elements.Count; i++)
        {
            if (elements[i].obj.activeInHierarchy)
            {
                if (first)
                {
                    elements[i].ToggleButtons(true, false);
                    first = false;
                }
                else
                    elements[i].ToggleButtons(true, true);
            }

            elements[i].obj.transform.SetSiblingIndex(i);
        }
    }

    public List<KeyValuePair<string, string>> GetFunctionsList()
    {
        List<KeyValuePair<string, string>> functions = new List<KeyValuePair<string, string>>();

        foreach (var element in elements)
        {
            if (element.obj.activeInHierarchy)
            {
                string method = game.functionsMatcher.GetFunction(element.txt.text);
                string output = element.txt.text.Split('(', ')')[1];

                var key = new KeyValuePair<string, string>(method, output);
                functions.Add(key);
            }
        }
        return functions;
    }

    public void ToggleLevels(bool enable)
    {
        if (enable)
            UpdateLevelsPanel();

        tutorialLevelsPanel.alpha = enable ? 1 : 0;
        tutorialLevelsPanel.blocksRaycasts = enable;
        tutorialLevelsPanel.interactable = enable;
    }

    public void ResetUI()
    {
        foreach (var element in elements)
            element.Reset();
    }

    public void UpdateLevelsPanel()
    {
        foreach (var level in levels)
        {
            string complited = "";
            Text txt = level.transform.Find("Text_Complited").GetComponent<Text>();

            if (level.levelSettings.isComplited)
            {
                txt.color = new Color(0.0235f, 0.702f, 0.09f, 1);
                complited = " UKOŃCZONO";
            }
            else
            {
                txt.color = new Color(1, 0, 0, 1);
                complited = " NIEUKOŃCZONO";
            }


            txt.text = complited;
        }
    }
    
    public void Follower()
    {
        if (previousElement != null)
            previousElement.ToggleFollower(false);

        ListElement listElement = elements[currentElement];
        listElement.ToggleFollower(true);

        previousElement = listElement;
        currentElement++;
    }

    public void DisableButtons()
    {
        foreach (var element in elements)
            element.DisableButtons();
    }
}

[System.Serializable]
public class ListElement
{
    private TutorialUI tutorialUI;

    private Button buttonDown;
    private Button buttonUp;
    private Button buttonDelete;

    public int id = -1;
    public GameObject obj;
    public Text txt;

    private Image follower1;
    private Image follower2;

    public ListElement(int id, GameObject obj, Text txt, TutorialUI tutorialUI)
    {
        this.id = id;
        this.obj = obj;
        this.txt = txt;
        this.tutorialUI = tutorialUI;

        obj.SetActive(false);

        buttonDown = obj.transform.Find("Buttons/Button_Down").GetComponent<Button>();
        buttonUp = obj.transform.Find("Buttons/Button_Up").GetComponent<Button>();
        buttonDelete = obj.transform.Find("Buttons/Button_Delete").GetComponent<Button>();

        follower1 = obj.transform.Find("Follower").GetComponent<Image>();
        follower2 = obj.transform.Find("Follower_Arrow").GetComponent<Image>();

        buttonDown.onClick.AddListener(delegate { Down(); });
        buttonUp.onClick.AddListener(delegate { Up(); });
        buttonDelete.onClick.AddListener(delegate { Delete(); });
    }

    public void Reset()
    {
        id = -1;
        obj.SetActive(false);
        txt.text = "";
    }

    public void ToggleButtons(bool buttonDown, bool buttonUp)
    {
        this.buttonDown.gameObject.SetActive(buttonDown);
        this.buttonUp.gameObject.SetActive(buttonUp);
        this.buttonDelete.gameObject.SetActive(true);
    }

    public void DisableButtons()
    {
        buttonDown.gameObject.SetActive(false);
        buttonUp.gameObject.SetActive(false);
        buttonDelete.gameObject.SetActive(false);
    }

    public bool IsActive()
    {
        return obj.activeInHierarchy;
    }

    public void Set(string text)
    {
        txt.text = text;
        obj.SetActive(true);
    }

    public void Down()
    {
        tutorialUI.Move(this, 1);
    }

    public void Up()
    {
        tutorialUI.Move(this, -1);
    }

    public void Delete()
    {
        obj.SetActive(false);
        tutorialUI.RefreshList();
    }

    public void ToggleFollower(bool enable)
    {
        follower1.enabled = enable;
        follower2.enabled = enable;
    }
}