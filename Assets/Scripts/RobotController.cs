﻿using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class RobotController : MonoBehaviour
{
    public RobotMovement movement;

    public Vector3 GetPosition { get { return transform.position; } }
    public bool IsDestroyed { get { return isDestroyed; } }

    private GameController game;
    private AudioSource audioSource;
    private bool isDestroyed = false;

    private void Start()
    {
        game = Managers.instance.game;
        movement = GetComponent<RobotMovement>();

        audioSource = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enviro" && !isDestroyed)
            SelffDestruction();
    }

    public void SetRobotPositionAndRotation(Vector3 pos, Vector3 rot)
    {
        transform.position = pos;

        Quaternion rotation = Quaternion.identity;
        rotation.eulerAngles = rot;

        transform.rotation = rotation;
    }

    private void SelffDestruction()
    {
        movement.StopTransforming();

        GameObject destructionParticle = Resources.Load("Particle/SelffDestruction", typeof(GameObject)) as GameObject;
        if (destructionParticle != null)
        {
            Transform inst = Instantiate(destructionParticle, transform).transform;
            inst.transform.localPosition = new Vector3(0, 0, 0);

            if (transform.GetChild(0).GetComponent<MeshRenderer>())
                transform.GetChild(0).GetComponent<MeshRenderer>().enabled = false;

            AudioClip destructionAudio = Resources.Load("Audio/SelffDestruction", typeof(AudioClip)) as AudioClip;
            if (destructionAudio != null)
                SetAudio(destructionAudio);
            else
                Debug.LogError("There is no AUDIO 'SelffDestruction' in the resources. Path: Audio/SelffDestruction");

            game.EndGame(true);
        }
        else
            Debug.LogError("There is no PARTICLE 'SelffDestruction' in the resources. Path: Particle/SelffDestruction");

        isDestroyed = true;
    }

    private void SetAudio(AudioClip clip)
    {
        if (audioSource != null)
        {
            if (audioSource.isPlaying)
                audioSource.Stop();

            audioSource.clip = clip;
            audioSource.Play();
        }
    }
}
