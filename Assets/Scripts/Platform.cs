﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour
{
    [Header("Displacement settings")]
    public Vector3 displacementVector = Vector3.zero;
    public float speed = 1;

    private Vector3 targetPosition = Vector2.zero;
    private bool isMoving = false;
    private bool revert = false;

    private void Update()
    {
        Moving();
    }

    private void Moving()
    {
        if (isMoving)
        {
            transform.position = Vector3.MoveTowards(transform.position, targetPosition, Time.deltaTime * speed);
            if (transform.position == targetPosition)
                isMoving = false;
        }
    }

    public void Move()
    {
        if (!isMoving)
        {
            if (revert)
                targetPosition = transform.position - displacementVector;
            else
                targetPosition = transform.position + displacementVector;

            revert = !revert;
            isMoving = true;
        }
    }
}