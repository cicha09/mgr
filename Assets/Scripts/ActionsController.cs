﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine.Events;
using UnityEngine;

public class ActionsController : MonoBehaviour
{
    private GameController game;
    private List<UnityAction> actions = new List<UnityAction>();

    private float actionTimer = .8f;
    private bool playerRound = true;

    private void Start()
    {
        game = Managers.instance.game;
    }

    public void MakeActionList(List<Code> codes)
    {
        actions = new List<UnityAction>();

        foreach (Code code in codes)
        {
            if (code.codeBlock == null)
            {
                string actionMethodName = code.function.Key;
                string value = code.function.Value;

                if (actionMethodName == "move")
                    actions.Add(delegate { SetRobotMove(1); });
                else if (actionMethodName == "rotate")
                    actions.Add(delegate { SetRobotRotate(90); });
                else if (actionMethodName == "rotateLeft")
                    actions.Add(delegate { SetRobotRotate(-90); });
                else if (actionMethodName == "rotateRight")
                    actions.Add(delegate { SetRobotRotate(90); });
                else if (actionMethodName == "moveBy")
                    actions.Add(delegate { SetRobotMove(int.Parse(value)); });
                else if (actionMethodName == "")
                    actions.Add(null);
                else
                    Debug.LogWarning("Wrong method name.");
            }
            else
            {
                if (code.codeBlock.condition == "true")
                {
                    foreach (KeyValuePair<string, string> key in code.codeBlock.functions)
                    {
                        string actionMethodName = key.Key;
                        string value = key.Value;

                        if (actionMethodName == "move")
                            actions.Add(delegate { SetRobotMove(1); });
                        else if (actionMethodName == "rotate")
                            actions.Add(delegate { SetRobotRotate(90); });
                        else if (actionMethodName == "rotateLeft")
                            actions.Add(delegate { SetRobotRotate(-90); });
                        else if (actionMethodName == "rotateRight")
                            actions.Add(delegate { SetRobotRotate(90); });
                        else if (actionMethodName == "moveBy")
                            actions.Add(delegate { SetRobotMove(int.Parse(value)); });
                        else if (actionMethodName == "")
                            actions.Add(null);
                        else
                            Debug.LogWarning("Wrong method name.");
                    }
                }
            }
        }
    }

    public void MakeActionList(List<KeyValuePair<string, string>> methodsNames)
    {
        playerRound = true;
        actions = new List<UnityAction>();
        List<KeyValuePair<string, string>> keys = new List<KeyValuePair<string, string>>();
        foreach (KeyValuePair<string, string> key in methodsNames)
        {
            string actionMethodName = key.Key;
            string value = key.Value;

            if (actionMethodName == "move")
                actions.Add(delegate { SetRobotMove(1); });
            else if (actionMethodName == "rotate")
                actions.Add(delegate { SetRobotRotate(90); });
            else if (actionMethodName == "rotateLeft")
                actions.Add(delegate { SetRobotRotate(-90); });
            else if (actionMethodName == "rotateRight")
                actions.Add(delegate { SetRobotRotate(90); });
            else if (actionMethodName == "moveBy")
                actions.Add(delegate { SetRobotMove(int.Parse(value)); });
            else if (actionMethodName == "")
                actions.Add(null);
            else
                Debug.LogWarning("Wrong method name.");
        }
    }

    public void StartActions()
    {
        if (actions.Count > 0)
        {
            if (!game.gameSet.levelSettings.tutorial)
                CodeEditor.instance.ui.GetFollowerReady();
            StartCoroutine(EndActionWaiter());
        }
    }

    IEnumerator EndActionWaiter()
    {
        for (int i = 0; i < actions.Count; i++)
        {
            game.hudController.gameUI.SetRoundIcon(playerRound);
            if (playerRound)
            {
                if (!game.gameSet.levelSettings.tutorial)
                {
                    if (actions[i] == null)
                        CodeEditor.instance.ui.MoveFollower(false);
                    else
                        CodeEditor.instance.ui.MoveFollower(true);
                }
                else
                {
                    if (actions[i] != null)
                        game.tutorialController.ui.Follower();
                }

                yield return new WaitForSeconds(1);
                if (actions[i] != null)
                    actions[i].Invoke();
            }
            else
            {
                BoardAction();
                i--;
            }
            playerRound = !playerRound;
            yield return new WaitForSeconds(actionTimer);
        }

        CheckResult();
    }

    private void BoardAction()
    {
        game.boardController.SetAction();
    }

    private void SetRobotMove(int steps)
    {
        game.AddMoveToGameStat();
        game.robot.movement.MoveForward(steps);
    }

    private void SetRobotRotate(int rotation)
    {
        game.AddMoveToGameStat();
        game.robot.movement.Rotate(rotation);
    }

    private void CheckResult()
    {
        bool reachedDestinationTile = game.robot.movement.ReachedDestinationTile();
        game.EndGame(!reachedDestinationTile);

    }
}