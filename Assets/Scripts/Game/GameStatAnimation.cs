﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class GameStatAnimation : MonoBehaviour
{
    [Header("Settings")]
    public Text animatedTextField = null;
    public float animatedValue = 0;
    public int decimalPlaces = 0;
    public bool isTime = false;

    private float animationSpeed = 10, value = 0;
    private bool animate = false;

    private void Update()
    {
        Animate();
    }

    public void StartAnimation()
    {
        if (animatedTextField != null)
            animate = true;
    }

    private void Animate()
    {
        if (animate)
        {
            value = Mathf.MoveTowards(value, animatedValue, Time.deltaTime * animationSpeed);

            if (isTime)
            {
                int minutes = Mathf.FloorToInt(value / 60F);
                int seconds = Mathf.FloorToInt(value - minutes * 60);

                animatedTextField.text = string.Format("{0:00}:{1:00}", minutes, seconds);
            }
            else
                animatedTextField.text = System.Math.Round(value, decimalPlaces).ToString();

            if (value == animatedValue)
            {
                animate = false;
                Destroy(this);
            }
        }
    }
}
