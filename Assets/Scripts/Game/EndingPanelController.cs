﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class EndingPanelController : MonoBehaviour
{
    [Header("Panels")]
    public CanvasGroup canvasGroup;
    public CanvasGroup gameOverPanel;
    public CanvasGroup winPanel;

    [Header("Buttons")]
    public EventTrigger btn_retry = null;
    public EventTrigger btn_chooseLevel = null;
    public EventTrigger btn_mainMenu = null;

    [Header("Stats")]
    public Text timeStatField = null;
    public Text movesStatFiled = null;

    private GameController game;

    private void Start()
    {
        game = Managers.instance.game;

        SetButtons();
        DisableAllPanels();
    }

    public void SetEndGame(GameStats gameStats)
    {
        if (gameStats != null)
        {
            EnablePanel(canvasGroup);

            if (gameStats.IsGameOver)
                EnablePanel(gameOverPanel);
            else
                EnablePanel(winPanel);

            FillStatsFields(gameStats);
        }
        else
            Debug.LogWarning("Game Statistics are null.");
    }

    private void EnablePanel(CanvasGroup canvasGroup)
    {
        canvasGroup.alpha = 1;
        canvasGroup.blocksRaycasts = true;
    }

    private void DisablePanel(CanvasGroup canvasGroup)
    {
        canvasGroup.alpha = 0;
        canvasGroup.blocksRaycasts = false;
    }

    private void DisableAllPanels()
    {
        DisablePanel(canvasGroup);
        DisablePanel(gameOverPanel);
        DisablePanel(winPanel);
    }

    private void FillStatsFields(GameStats gameStats)
    {
        FillStat(timeStatField, gameStats.Time, 2, true);
        FillStat(movesStatFiled, gameStats.Moves, 0, false);
    }

    private void FillStat(Text animatedTextField, float animatedValue, int decimalPlaces, bool isTime)
    {
        GameObject animatedStatObject = new GameObject();
        animatedStatObject.name = "AnimatedStat_" + animatedTextField.name;

        GameStatAnimation animatedStat = animatedStatObject.AddComponent<GameStatAnimation>();
        animatedStat.animatedTextField = animatedTextField;
        animatedStat.animatedValue = animatedValue;
        animatedStat.decimalPlaces = decimalPlaces;
        animatedStat.isTime = isTime;
        animatedStat.StartAnimation();
    }

    private void SetButtons()
    {
        EventTrigger.Entry pointerDownEntry = new EventTrigger.Entry();
        pointerDownEntry.eventID = EventTriggerType.PointerDown;

        pointerDownEntry.callback.AddListener((data) => { RetryGame(); });
        pointerDownEntry.callback.AddListener((data) => { DisableAllPanels(); });
        btn_retry.triggers.Add(pointerDownEntry);

        pointerDownEntry = new EventTrigger.Entry();
        pointerDownEntry.eventID = EventTriggerType.PointerDown;
        pointerDownEntry.callback.AddListener((data) => { ChooseLevel(); });
        pointerDownEntry.callback.AddListener((data) => { DisableAllPanels(); });
        btn_chooseLevel.triggers.Add(pointerDownEntry);

        pointerDownEntry = new EventTrigger.Entry();
        pointerDownEntry.eventID = EventTriggerType.PointerDown;
        pointerDownEntry.callback.AddListener((data) => { BackToMainMenu(); });
        pointerDownEntry.callback.AddListener((data) => { DisableAllPanels(); });
        btn_mainMenu.triggers.Add(pointerDownEntry);
    }

    public void BackToMainMenu()
    {
        game.mainMenu.ToggleVisibility(true, true);
        if (game.gameSet.levelSettings.tutorial)
            game.hudController.uiController.RetryTurorialWhenChooseLevel();
    }

    private void ChooseLevel()
    {
        if (game.gameSet.levelSettings.tutorial)
        {
            game.tutorialController.ui.ToggleLevels(true);
            game.hudController.uiController.RetryTurorialWhenChooseLevel();
        } else
        {
            game.hudController.levelsPanel.ToggleVisibility(true);
        }
    }

    private void RetryGame()
    {
        if (game.gameSet.levelSettings.tutorial)
            game.hudController.uiController.RetryTutorialGame();
        else
        {
            game.RetryGame();
        }
    }
}