﻿public class GameStats
{
    private int moves = 0;
    private float time = 0.0f;
    private bool isGameOver = false;

    public int Moves { get; set; }
    public float Time { get; set; }
    public bool IsGameOver { get; set; }
}