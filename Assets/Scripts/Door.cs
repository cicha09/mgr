﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    public enum Axis { X, Z };
    public Axis axis = Axis.Z;

    private GameController game;
    private Animator animator;

    private void Start()
    {
        game = Managers.instance.game;

        if (GetComponent<Animator>())
            animator = GetComponent<Animator>();
    }

    public void MoveDoor()
    {
        if (GetDistance() < 1.5f)
            Open();
        else
            Close();
    }

    private void Open()
    {
        if (animator != null)
        {
            animator.ResetTrigger("Close");
            animator.SetTrigger("Open");
        }
    }

    public void Close()
    {
        if (animator != null)
        {
            animator.ResetTrigger("Open");
            animator.SetTrigger("Close");
        }
    }

    public float GetDistance()
    {
        Vector3 robotPosition = game.robot.GetPosition;

        float distance = 0;

        if (axis == Axis.Z)
            distance = Mathf.Abs((robotPosition - transform.position).z);
        else if (axis == Axis.X)
            distance = Mathf.Abs((robotPosition - transform.position).x);

        return distance;
    }
}