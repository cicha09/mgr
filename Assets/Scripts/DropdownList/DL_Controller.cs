﻿using System.Collections.Generic;
using UnityEngine;

public class DL_Controller : MonoBehaviour
{
    private List<DL_Element> elements = new List<DL_Element>();

    public void AddElements(DL_Element dL_Element)
    {
        if (!elements.Contains(dL_Element))
            elements.Add(dL_Element);
    }

    public void ToggleAllOff(DL_Element dl_Element)
    {
        foreach (var elem in elements)
        {
            if (elem != dl_Element && elem.IsToggled)
                elem.ToggleOff();
        }
    }
}