﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DL_Element : MonoBehaviour
{
    private DL_Controller dl_controller;

    public bool IsToggled { get { return content.activeInHierarchy; } }
    public GameObject content;

    private bool secondPressed = false;

    private void Start()
    {
        dl_controller = transform.parent.parent.GetComponent<DL_Controller>();
        dl_controller.AddElements(this);
    }

    public void ToggleOn()
    {
        if (!secondPressed)
        {
            secondPressed = true;
            dl_controller.ToggleAllOff(this);
            content.SetActive(true);
        } else
            ToggleOff();
    }

    public void ToggleOff()
    {
        secondPressed = false;
        content.SetActive(false);
    }
}