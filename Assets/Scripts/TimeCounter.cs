﻿using UnityEngine;
using UnityEngine.UI;

public class TimeCounter : MonoBehaviour
{
    private bool counting = false;
    private float timer = 0.0f;
    private Text field;
    private GameController gameController;

    private void Update()
    {
        CountTime();
    }

    private void CountTime()
    {
        if (counting)
        {
            timer -= Time.deltaTime;

            int minutes = Mathf.FloorToInt(timer / 60F);
            int seconds = Mathf.FloorToInt(timer - minutes * 60);

            field.text = string.Format("{0:00}:{1:00}", minutes, seconds);

            if (timer <= 0.1f)
            {
                counting = false;
                gameController.TimeOut();
            }
        }
    }

    public void StartCount(Text field, float timer, GameController gameController)
    {
        this.timer = timer;
        this.field = field;
        this.gameController = gameController;

        counting = true;
    }

    public float StopCountAndReturnTime()
    {
        field.transform.parent.GetComponent<Image>().color = Color.grey;
        counting = false;
        return timer;
    }
}