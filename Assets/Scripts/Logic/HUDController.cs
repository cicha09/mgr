﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HUDController : MonoBehaviour
{
    public GameUIController gameUI;
    public LevelsPanelController levelsPanel;
    public EndingPanelController endingPanel;
    public UIController uiController;

    private void Start()
    {
        gameUI = FindObjectOfType<GameUIController>();
        levelsPanel = FindObjectOfType<LevelsPanelController>();
        endingPanel = FindObjectOfType<EndingPanelController>();
        uiController = FindObjectOfType<UIController>();
    }
}