﻿using UnityEngine;

public class Managers : MonoBehaviour
{
    public static Managers instance;

    public GameController game;

    private void Awake()
    {
        if (instance)
            Destroy(this);
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }

        game = GetComponent<GameController>();
    }
}
