﻿using System.Collections.Generic;
using UnityEngine;

public class MainMenuController : MonoBehaviour
{
    public GameController game;

    public GameObject environmentContent;
    public Transform cameraTransform;

    private CanvasGroup canvasGroup = null;

    private void Start()
    {
        game = Managers.instance.game;

        canvasGroup = GetComponent<CanvasGroup>();
        ToggleVisibility(true, true);
    }

    public void ToggleVisibility(bool enablePanel, bool enableContent)
    {
        if (enablePanel)
        {
            Enable();
            game.gameCamera.movement.Displace(cameraTransform.position, cameraTransform.rotation);
        }
        else
            Disable();

        if (environmentContent != null)
            environmentContent.SetActive(enableContent);
    }

    private void Disable()
    {
        canvasGroup.alpha = 0;
        canvasGroup.blocksRaycasts = false;
    }

    private void Enable()
    {
        canvasGroup.alpha = 1;
        canvasGroup.blocksRaycasts = true;
    }

    #region Buttons
    public void StartGame()
    {
        ToggleVisibility(false, true);
        game.typing.ui.EnablePanel(new List<int>() { 5, 6 }, AfterTypingAction);
    }

    public void StartTutorial()
    {
        ToggleVisibility(false, true);
        game.tutorialController.ui.ToggleLevels(true);
        //game.typing.ui.EnablePanel(new List<int>() { 18 }, EnableTutorial);
    }

    private void AfterTypingAction()
    {
        game.hudController.levelsPanel.ToggleVisibility(true);
    }

    private void EnableTutorial()
    {
        game.tutorialController.Toggle(true);
    }

    public void Options()
    {

    }

    public void Exit()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }
    #endregion
}