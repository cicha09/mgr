﻿using UnityEngine;
using UnityEngine.UI;

public class GameUIController : MonoBehaviour
{
    public MethodsUIPanel methodsUI;

    public Text editorHeader;
    public Text editorCounter;

    [Header("Round Panel")]
    public Image enemyIcon;
    public Image playerIcon;

    [Header("Elements")]
    public InputField codeEditorFiled;
    public InputField tutorialCodeEditorField;
    public Text timerField;
    public CanvasGroup canvasGroup;

    private GameController game;

    private Sprite currentRoundSprite = null;
    private Sprite normalSprite = null;

    private void Start()
    {
        LoadSprites();
        game = Managers.instance.game;
        methodsUI = GetComponent<MethodsUIPanel>();
        ToggleVisibility(false);
    }

    private void LoadSprites()
    {
        currentRoundSprite = Resources.Load("UI/currentRoundSprite", typeof(Sprite)) as Sprite;
        normalSprite = Resources.Load("UI/roundNormalSprite", typeof(Sprite)) as Sprite;
    }

    public void PauseTime()
    {
        timerField.text = "--:--";
    }

    public void SetRoundIcon(bool playerRound)
    {
        if (playerRound)
        {
            playerIcon.sprite = currentRoundSprite;
            enemyIcon.sprite = normalSprite;
        }
        else
        {
            playerIcon.sprite = normalSprite;
            enemyIcon.sprite = currentRoundSprite;
        }
    }

    public void SetGamePanel()
    {
        ToggleVisibility(true);
        codeEditorFiled.Select();
    }

    public void SetTutorialGamePanel()
    {
        canvasGroup.alpha = 1;
        editorHeader.text = game.gameSet.levelSettings.header;
        editorCounter.text = (game.gameSet.levelSettings.id + 1) + "/" + game.tutorialController.tutorials.Count;
    }

    public void LockGamePanel()
    {

    }

    public void UnsetGamePanel()
    {
        ToggleVisibility(false);
    }

    public void PreparePanel(LevelSettings levelSettings)
    {
        if (levelSettings.tutorial)
        {
            codeEditorFiled.gameObject.SetActive(false);
            tutorialCodeEditorField.gameObject.SetActive(true);
        }
        else
        {
            codeEditorFiled.gameObject.SetActive(true);
            tutorialCodeEditorField.gameObject.SetActive(false);
        }

        CodeEditor.instance.ui.PrepareEditor();
    }

    public void ToggleVisibility(bool enable)
    {
        if (enable)
            EnablePanel();
        else
            DisablePanel();
    }

    private void EnablePanel()
    {
        if (game.gameSet.levelSettings.timer == 0)
            PauseTime();

        canvasGroup.alpha = 1;
        ToggleInteractable(true);
    }

    private void DisablePanel()
    {
        canvasGroup.alpha = 0;
        ToggleInteractable(false);
    }

    public void ToggleInteractable(bool interactable)
    {
        canvasGroup.blocksRaycasts = interactable;
        canvasGroup.interactable = interactable;
    }

    #region Buttons
    public void Compile()
    {
        CodeEditor.instance.CompileCode(game.gameSet.levelSettings.tutorial);
        game.EndEditing();
    }

    public void Exit()
    {
        game.hudController.gameUI.UnsetGamePanel();
        game.hudController.endingPanel.BackToMainMenu();
    }
    #endregion
}