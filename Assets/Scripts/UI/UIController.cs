﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIController : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
    [Header("test")]
    public bool test = false;
    public LevelSettings levelSet;

    private GameController game;
    private Text currentButtonText = null;
    private Outline currentOutline = null;
    private bool infoIsSet = false;

    private void Start()
    {
        game = Managers.instance.game;

        if (test)
        {
            game.hudController.levelsPanel.ToggleVisibility(false);
            game.SetGame(levelSet);
            game.mainMenu.ToggleVisibility(false, false);
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (eventData.pointerCurrentRaycast.gameObject != null)
        {
            GameObject currentHover = eventData.pointerCurrentRaycast.gameObject;
            if (currentHover != null)
            {
                SetMouseEnterAudio();
                if (currentHover.GetComponent<Text>())
                {
                    currentButtonText = currentHover.GetComponent<Text>();
                    currentButtonText.color = Color.gray;
                }
                else if (currentHover.GetComponent<Image>())
                {
                    currentOutline = currentHover.GetComponent<Outline>();
                    currentOutline.effectColor = Color.white;


                    if (currentOutline.GetComponent<LevelController>())
                    {
                        LevelSettings ls = currentOutline.GetComponent<LevelController>().levelSettings;
                        game.hudController.levelsPanel.SetInfoText(ls);
                        infoIsSet = true;
                    }
                }
            }
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (currentButtonText != null)
        {
            SetMouseExitAudio();
            currentButtonText.color = Color.white;
        }
        if (currentOutline != null)
        {
            SetMouseExitAudio();
            currentOutline.effectColor = Color.black;

            if (infoIsSet)
            {
                game.hudController.levelsPanel.SetInfoText("");
                infoIsSet = false;
            }
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.pointerCurrentRaycast.gameObject != null)
        {
            SetMouseDownAudio();
            GameObject currentClicked = eventData.pointerCurrentRaycast.gameObject;
            if (currentClicked != null)
            {
                if (currentClicked.GetComponent<LevelController>())
                {
                    LevelSettings lvlSet = currentClicked.GetComponent<LevelController>().levelSettings;

                    if (lvlSet.tutorial == false)
                        game.hudController.levelsPanel.ToggleVisibility(false);
                    else
                    {
                        game.tutorialController.ui.ToggleLevels(false);

                        if (lvlSet.id == 0)
                            game.tutorialController.Toggle(true);
                        else
                            game.tutorialController.SetTutorialPanel(lvlSet.id);
                    }

                    game.SetGame(lvlSet);
                    game.mainMenu.ToggleVisibility(false, false);
                }
                else if (currentClicked.GetComponent<DL_Element>())
                {
                    currentClicked.GetComponent<DL_Element>().ToggleOn();
                }
            }
        }
    }

    public void RetryTutorialGame()
    {
        game.tutorialController.SetTutorialPanel(game.gameSet.levelSettings.id);
        game.tutorialController.ui.ResetUI();
        game.SetGameRetry(game.gameSet.levelSettings);
        game.boardController.ResetMovables();
        CodeEditor.instance.ui.PrepareEditor();
    }

    public void RetryTurorialWhenChooseLevel()
    {
        game.tutorialController.ui.ResetUI();
        CodeEditor.instance.ui.PrepareEditor();
    }

    #region Audio
    public void SetMouseEnterAudio()
    {
        game.gameAudio.SetMouseEvent(AudioController.MouseEvents.enter);
    }

    public void SetMouseExitAudio()
    {
        game.gameAudio.SetMouseEvent(AudioController.MouseEvents.exit);
    }

    public void SetMouseDownAudio()
    {
        game.gameAudio.SetMouseEvent(AudioController.MouseEvents.down);
    }
    #endregion
}
