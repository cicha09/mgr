﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(TypingController))]
public class TypingPanelController : MonoBehaviour
{
    public Text textField;
    public Text tutorialTextField;
    public EventTrigger skipButton, skipTutorialButton;

    public GameObject tutorialWindow;
    public GameObject gameWindow;

    private GameController game;
    private TypingController typingContoller;
    private CanvasGroup canvasGroup;
    private List<UnityAction> actions;
    private UnityAction endTypingAction;

    private void Start()
    {
        game = Managers.instance.game;
        canvasGroup = GetComponent<CanvasGroup>();
        typingContoller = GetComponent<TypingController>();
        DisablePanel();

        FillButtons();
    }

    public void EnablePanel(List<int> keys, UnityAction endTypingAction)
    {
        this.endTypingAction = endTypingAction;

        canvasGroup.alpha = 1;
        canvasGroup.blocksRaycasts = true;

        gameWindow.SetActive(true);

        typingContoller.SetTyping(keys, textField);
    }

    public void EnableTutorialPanel(int tutorialNumber)
    {
        canvasGroup.alpha = 1;
        canvasGroup.blocksRaycasts = true;

        Tutorial tutorial = game.tutorialController.GetTutorial(tutorialNumber);

        tutorialWindow.SetActive(true);
        typingContoller.SetTyping(tutorial.textIDs, tutorialTextField);
        game.tutorialController.video.PlayVideo(tutorial);
    }

    public void DisablePanel()
    {
        canvasGroup.alpha = 0;
        canvasGroup.blocksRaycasts = false;

        tutorialWindow.SetActive(false);
        gameWindow.SetActive(false);
    }

    private void FillButtons()
    {
        EventTrigger.Entry pointerDownEntry = new EventTrigger.Entry();
        pointerDownEntry.eventID = EventTriggerType.PointerDown;
        pointerDownEntry.callback.AddListener((data) => { SkipAction(); });

        skipButton.triggers.Add(pointerDownEntry);

        pointerDownEntry = new EventTrigger.Entry();
        pointerDownEntry.eventID = EventTriggerType.PointerDown;
        pointerDownEntry.callback.AddListener((data) => { SkipTutorialAction(); });
        skipTutorialButton.triggers.Add(pointerDownEntry);
    }

    private void SkipTutorialAction()
    {
        DisablePanel();
        game.hudController.gameUI.ToggleVisibility(true);
        //game.SetGame(game.tutorialController.CurrentTutorial.levelSettings);
    }

    private void SkipAction()
    {
        if (!typingContoller.CheckNextTyping())
        {
            DisablePanel();

            if (endTypingAction != null)
            {
                endTypingAction.Invoke();
                endTypingAction = null;
            }
        }
        else
            typingContoller.SetNextTyping();
    }
}