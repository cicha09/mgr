﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TypingController : MonoBehaviour
{
    public TypingPanelController ui;

    private GameController game;
    private List<TypingSettings> typingSettings;
    private float pause = 0.03f;
    private int counter = 0;

    private void Start()
    {
        game = Managers.instance.game;
        ui = GetComponent<TypingPanelController>();
    }

    public void SetTyping(List<int> keys, Text textField)
    {
        FillLists(keys, textField);
        counter = 0;
        StartTyping();
    }

    public bool CheckNextTyping()
    {
        if (typingSettings.Count == counter + 1)
            return true;
        else
        {
            StopTypingAudio();
            StopAllCoroutines();
            return false;
        }
    }

    public void SetNextTyping()
    {
        StartTyping();
    }

    private void FillLists(List<int> keys, Text textField)
    {
        typingSettings = new List<TypingSettings>();

        foreach (var key in keys)
            typingSettings.Add(new TypingSettings(textField, game.localization.GetLocalizedValue(key)));
    }

    private void StartTyping()
    {
        if (counter < typingSettings.Count)
        {
            StartTypingAudio();
            TypingSettings typSet = typingSettings[counter];
            typSet.textField.text = "";

            StopAllCoroutines();
            StartCoroutine(Typing(typSet.textField, typSet.textString));

            counter++;
        }
    }

    private IEnumerator Typing(Text textField, string stringText)
    {
        foreach (char c in stringText)
        {
            textField.text += c;
            yield return new WaitForSeconds(pause);
        }

        StopTypingAudio();
    }

    #region Audio
    private void StartTypingAudio()
    {
        game.gameAudio.SetTyping();
    }

    private void StopTypingAudio()
    {
        game.gameAudio.StopUIAudio();
    }
    #endregion
}

public class TypingSettings
{
    public Text textField;
    public string textString;

    public TypingSettings(Text textField, string textString)
    {
        this.textField = textField;
        this.textString = textString;
    }
}