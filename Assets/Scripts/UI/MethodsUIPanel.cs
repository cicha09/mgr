﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MethodsUIPanel : MonoBehaviour
{
    public Transform methodsTransformParent;

    private GameController game;
    private Dictionary<string, GameObject> panels;

    public void Start()
    {
        game = Managers.instance.game;

        GetPanels();
        SetButtons();
    }

    private void GetPanels()
    {
        panels = new Dictionary<string, GameObject>();

        foreach (Transform panel in methodsTransformParent.transform)
        {
            if (panel.gameObject.activeInHierarchy)
            {
                panels.Add(panel.name, panel.gameObject);
                panel.gameObject.SetActive(false);
            }
        }
    }

    private void SetButtons()
    {
        foreach (GameObject panel in panels.Values)
        {
            Button btn = panel.transform.Find("use").GetComponent<Button>();
            InputField inputField = panel.transform.Find("function").GetComponentInChildren<InputField>();

            if (inputField)
                btn.onClick.AddListener(delegate { Use(panel.name, inputField.text); });
            else
                btn.onClick.AddListener(delegate { Use(panel.name); });
        }
    }

    private void DisableAllPanels()
    {
        foreach (var panel in panels.Values)
        {
            panel.transform.Find("use").gameObject.SetActive(true);
            panel.SetActive(false);
        }
    }

    public void PreapareMethodList(List<string> methods, bool tutorial)
    {
        DisableAllPanels();

        foreach (var panel in panels)
        {
            if (methods.Contains(panel.Key))
            {
                panel.Value.SetActive(true);
                if (!tutorial)
                    panel.Value.transform.Find("use").gameObject.SetActive(false);
            }
        }
    }

    public void Use(string methodName)
    {
        game.tutorialController.ui.AddMethodToTutorialEditor(methodName);
    }

    public void Use(string methodName, string parametr)
    {
        game.tutorialController.ui.AddMethodToTutorialEditor(methodName, parametr);
    }
}