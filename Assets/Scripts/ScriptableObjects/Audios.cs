﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Audios", menuName = "Audio Container", order = 1)]
public class Audios : ScriptableObject
{

    [Space(10), Header("UI")]
    public AudioClip keyboardTyping;

    [Space(10), Header("UI - Buttons")]
    public AudioClip mouseEnter;
    public AudioClip mouseExit;
    public AudioClip mouseDown;
}