﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

[CreateAssetMenu(menuName = "Tutorial/Tutorial Settings")]
public class Tutorial : ScriptableObject
{
    public int id;
    public LevelSettings levelSettings;

    [Header("Video")]
    public VideoClip videoClip;

    [Header("Texts")]
    public List<int> textIDs = new List<int>();
}