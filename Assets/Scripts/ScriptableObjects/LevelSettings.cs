﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "LevelSettings", menuName = "Level Settings", order = 1)]
public class LevelSettings : ScriptableObject
{
    public int id = -1;
    public bool tutorial = false;
    public float timer = 0;
    public bool isComplited = false;
    public string header = "";

    [Tooltip("List should be in a specific order")]
    public List<int> typingTextKeys = new List<int>();

    [Header("Board")]
    public GameObject contentPrefab;
    public Vector3 instantiateContentPosition = Vector3.zero;

    [Header("Robot")]
    public Vector3 robotPosition = Vector3.zero;
    public Vector3 robotRotation = Vector3.zero;

    [Tooltip("Methods: move, rotate, move(X)")]
    public List<string> methods = new List<string>();

    [Tooltip("Constructions: while")]
    public List<string> constructions = new List<string>();
}