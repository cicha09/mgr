﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

[CreateAssetMenu(fileName = "Strings", menuName = "Strings Container", order = 1)]
public class Strings : ScriptableObject
{
    [HideInInspector, SerializeField]
    public List<ResourceEntry> entries = new List<ResourceEntry>();

    [HideInInspector]
    public GameSettings gameSettings = null;

    public string GetValueByKey(int key)
    {
        if (gameSettings == null)
            gameSettings = Resources.Load("Data/GameSettings", typeof(GameSettings)) as GameSettings;

        var entry = entries.Where(s => s.string_id == key).Single();

        if (gameSettings.CurrentLanguage == "English")
            return entry.string_EN;
        else if (gameSettings.CurrentLanguage == "Polski")
            return entry.string_PL;
        else
            return "error";
    }
}

[System.Serializable]
public class ResourceEntry
{
    public int string_id;
    public string string_EN;
    public string string_PL;

    public ResourceEntry(int string_id, string string_EN, string string_PL)
    {
        this.string_id = string_id;
        this.string_EN = string_EN;
        this.string_PL = string_PL;
    }
}