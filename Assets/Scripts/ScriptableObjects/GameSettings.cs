﻿using System;
using UnityEngine;

[CreateAssetMenu(fileName = "GameSettings", menuName = "Game Settings", order = 1)]
public class GameSettings : ScriptableObject
{
    private enum Language { English, Polski };
    [SerializeField] private Language language = Language.Polski;

    public string CurrentLanguage {
        get { return language.ToString(); }
        set { language = (Language)Enum.Parse(typeof(Language), value); }
    }

    public ColorsSettings colorSettings;
}


[System.Serializable]
public class ColorsSettings
{
    public Color caret = Color.green;
}