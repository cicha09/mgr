﻿using System.Collections.Generic;
using UnityEngine;

public class BoardController : MonoBehaviour
{
    [Header("Destination tile")]
    public Material destinationTileMat;
    private GameObject destinationTile;

    private MovableObjects movableObjects;

    public void PrepareBoard(GameObject destinationTile, GameObject content)
    {
        this.destinationTile = destinationTile;
        destinationTile.GetComponent<MeshRenderer>().material = destinationTileMat;

        AddMovableObjects(content.transform);
    }

    public void SetAction()
    {
        if (movableObjects.Doors.Count > 0)
            movableObjects.Doors[0].MoveDoor();
    }

    public bool CheckDestinationTileReached(RaycastHit hit)
    {
        GameObject tile = hit.collider.gameObject;
        if (tile == destinationTile)
            return true;
        else
            return false;
    }

    public void ResetMovables()
    {
        if (movableObjects.Doors.Count > 0)
            movableObjects.Doors[0].Close();
    }

    private void AddMovableObjects(Transform movableObjectsParent)
    {
        movableObjects = new MovableObjects(movableObjectsParent);
    }
}

public class MovableObjects
{
    public List<Door> Doors { get; private set; }
    public List<Platform> Platforms { get; private set; }

    public MovableObjects(Transform content)
    {
        Doors = new List<Door>();
        Platforms = new List<Platform>();

        FindObjects(content);
    }

    private void FindObjects(Transform content)
    {
        var parent = content.Find("movable_objects");
        foreach (Transform child in parent)
        {
            if (child.GetComponent<Door>())
                Doors.Add(child.GetComponent<Door>());
            else if (child.GetComponent<Platform>())
                Platforms.Add(child.GetComponent<Platform>());
        }
    }
}