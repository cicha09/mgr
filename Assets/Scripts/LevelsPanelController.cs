﻿using UnityEngine;
using UnityEngine.UI;

public class LevelsPanelController : MonoBehaviour
{
    private GameController game;
    private CanvasGroup canvasGroup;

    private Text infoText;

    private void Start()
    {
        game = Managers.instance.game;

        canvasGroup = GetComponent<CanvasGroup>();
        infoText = transform.Find("LevelInfo").GetComponent<Text>();

        DisablePanel();
    }

    public void ToggleVisibility(bool enable)
    {
        if (enable) EnablePanel();
        else DisablePanel();
    }

    private void DisablePanel()
    {
        if (!canvasGroup)
            canvasGroup = GetComponent<CanvasGroup>();

        canvasGroup.alpha = 0;
        canvasGroup.blocksRaycasts = false;
        SetInfoText("");
    }

    private void EnablePanel()
    {
        canvasGroup.alpha = 1;
        canvasGroup.blocksRaycasts = true;
    }

    public void SetInfoText(string info)
    {
        infoText.text = info;
    }

    public void SetInfoText(LevelSettings settings)
    {
        string info = "<b>" + settings.header + "</b> [" + settings.id + "] \n";

        if (settings.timer != 0)
        {
            int minutes = Mathf.FloorToInt(settings.timer / 60F);
            int seconds = Mathf.FloorToInt(settings.timer - minutes * 60);
            info += "Limit czasowy: " + string.Format("{0:00}:{1:00} \n", minutes, seconds);
        }

        info += "Metody: ";
        int index = 0;
        foreach (string method in settings.methods)
        {
            index++;
            if (index == settings.methods.Count)
                info += method;
            else
                info += method + ", ";
        }

        index = 0;
        if (settings.constructions.Count != 0)
        {
            info += "\nKonstrukcje: ";
            foreach (string con in settings.constructions)
            {
                index++;
                if (index == settings.constructions.Count)
                    info += con;
                else
                    info += con + ", ";
            }
        }

        infoText.text = info;
    }

    #region Buttons
    public void Back()
    {
        ToggleVisibility(false);
        game.mainMenu.ToggleVisibility(true, true);
    }

    public void BackFromTutorialLevels()
    {
        game.tutorialController.ui.ToggleLevels(false);
        game.mainMenu.ToggleVisibility(true, true);
    }
    #endregion
}