﻿using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    public float delay = 1f;

    private Transform endPositionAndRotation;
    private bool isLerping = false;
    private float timeStartedLerping = 0;

    private void Update()
    {
        Lerping();
    }

    public void Displace(Vector3 displacePosition, Quaternion displaceRotation)
    {
        transform.position = displacePosition;
        transform.rotation = displaceRotation;
    }

    private void Lerping()
    {
        if (isLerping)
        {
            float timeSinceStarted = Time.time - timeStartedLerping;
            float percentageComplete = timeSinceStarted / delay;

            transform.transform.position = Vector3.Lerp(transform.transform.position, endPositionAndRotation.position, percentageComplete);
            transform.transform.rotation = Quaternion.Lerp(transform.transform.rotation, endPositionAndRotation.rotation, percentageComplete);

            if (percentageComplete >= 1.0f)
                isLerping = false;
        }
    }

    public void StartLerping(Transform targetTransform)
    {
        endPositionAndRotation = targetTransform;

        isLerping = true;
        timeStartedLerping = Time.time;
    }
}