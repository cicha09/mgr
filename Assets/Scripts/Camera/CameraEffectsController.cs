﻿using UnityEngine;

public class CameraEffectsController : MonoBehaviour
{
    private AmplifyColorEffect amplifyColor;
    private float amplifyColor_blendAmount = 0;
    private float amplifyColor_blendAmount_target = 0;
    private bool greyscaling = false;

    private void Start()
    {
        amplifyColor = GetComponent<AmplifyColorEffect>();
        amplifyColor_blendAmount = amplifyColor.BlendAmount;
    }

    private void Update()
    {
        if (greyscaling)
        {
            amplifyColor.BlendAmount = Mathf.MoveTowards(amplifyColor.BlendAmount, amplifyColor_blendAmount_target, Time.deltaTime * 0.2f);
            if (amplifyColor.BlendAmount == amplifyColor_blendAmount_target)
                greyscaling = false;
        }
    }

    public void ToggleGreyscale(bool enable)
    {
        amplifyColor_blendAmount_target = enable ? amplifyColor_blendAmount : 0;
        greyscaling = true;
    }
}