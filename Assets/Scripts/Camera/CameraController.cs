﻿using UnityEngine;

public class CameraController : MonoBehaviour
{
    public CameraEffectsController effects;
    public CameraMovement movement;
    public CameraShake shake;

    private void Awake()
    {
        effects = GetComponent<CameraEffectsController>();
        movement = GetComponent<CameraMovement>();
        shake = GetComponent<CameraShake>();
    }
}