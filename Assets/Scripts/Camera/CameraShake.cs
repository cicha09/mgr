﻿using UnityEngine;

public class CameraShake : MonoBehaviour
{
    public float shakeDuration = 0f;
    public float added = 0.1f;
    public float shakeAmount = 0.7f;
    public float decreaseFactor = 1.0f;

    private bool isShaking = false;
    private Vector3 originalPos;

    private void Update()
    {
        if (isShaking)
        {
            if (Input.GetKeyDown(KeyCode.V))
            {
                shakeDuration = added;
                originalPos = transform.localPosition;
            }

            if (shakeDuration > 0)
            {
                transform.localPosition = originalPos + Random.insideUnitSphere * shakeAmount;
                shakeDuration -= Time.deltaTime * decreaseFactor;
            }
            else
            {
                shakeDuration = 0f;
                transform.localPosition = originalPos;
            }
        }
    }
}