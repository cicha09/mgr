﻿using UnityEngine.UI;
using UnityEngine;

[RequireComponent(typeof(Text))]
public class LocalizedText : MonoBehaviour
{
    private GameController game;

    public int key = -1;

    private void Start()
    {
        game = Managers.instance.game;

        GetComponent<Text>().text = game.localization.GetLocalizedValue(key);
    }
}