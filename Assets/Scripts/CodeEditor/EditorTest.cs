﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;

public class EditorTest : MonoBehaviour
{
    public Text textField;
    public string codeLine = "";

    //private string pattern = "^robot.move\\(\\d\\d\\)\\;$";
   // private string pattern = "^robot.move\\(({1,2}\\d)\\)\\;$";

    private bool Match(string codeLine, string pattern)
    {
        Regex regex = new Regex(@"^robot.move\(\d{0,}\)\;$");// + pattern);

        if (regex.Match(codeLine).Success)
            return true;
        else
            return false;
    }

    public void ReadCode()
    {
        codeLine = textField.text;

        if (Match(codeLine, ""))
        {
            Debug.Log("OK!");
        }

        //// #1 Example Loop
        //WhileLoop whileLoop = new WhileLoop()
        //{
        //    Condition = "false",
        //    ExecuteCodes = "robot.move();",
        //    Executed = false
        //};

        //Condition condition = new Condition(whileLoop.Condition);
        //conditions.Add(condition);

        //int counter = 0; //!condition.Done() ||
        //while (!condition.Done())
        //{
        //    counter++;
        //    if (counter == 100)
        //    {
        //        Debug.Log("Counter equals 100");
        //        break;
        //    }
        //}

        //Debug.Log("Code compiled!");
    }

    private void CheckCondition()
    {

    }
}

[System.Serializable]
public class Condition
{
    public string condition = "";

    public Condition(string condition)
    {
        this.condition = condition;
    }

    public bool Done()
    {
        if (condition == "true")
            return true;
        else if (condition == "false")
            return false;
        else
        {
            Debug.Log("Condition is not 'true' and not 'false'");
            return false;
        }
    }
}