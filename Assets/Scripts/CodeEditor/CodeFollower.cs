﻿using System.Collections;
using UnityEngine.UI;
using UnityEngine;

public class CodeFollower : MonoBehaviour
{
    private RectTransform followerTransform;
    private Transform codeEditorField;

    private Color correctLineColor, wrongLineColor;
    private float addedValue = 0.05f;
    private CodeEditorUI editorUI;

    public CodeFollower(Transform codeEditorField, CodeEditorUI editorUI)
    {
        this.editorUI = editorUI;
        this.codeEditorField = codeEditorField;

        correctLineColor = new Color(0, 0.5f, 0, 0.37f);
        wrongLineColor = new Color(0.81f, 0.23f, 0.2f, 0.37f);
    }

    public void Instantinate()
    {
        GameObject followerObject = Resources.Load("UI/codeFollower", typeof(GameObject)) as GameObject;
        followerObject = Instantiate(followerObject, codeEditorField.transform);

        followerTransform = MatchTransform(followerObject);
        editorUI.follower = followerTransform;
    }

    public void MoveFollower(bool correctLine)
    {
        if (followerTransform != null)
        {
            float posY = followerTransform.anchoredPosition.y - 23;
            followerTransform.anchoredPosition = new Vector3(followerTransform.anchoredPosition.x, posY);
        }
        else
            Debug.LogWarning("Follower Transform not exist!");

        ChangeColor(correctLine);
    }

    private RectTransform MatchTransform(GameObject followerObject)
    {
        RectTransform rectTransform = followerObject.GetComponent<RectTransform>();
        rectTransform.sizeDelta = codeEditorField.GetComponent<RectTransform>().sizeDelta;

        Text t = codeEditorField.GetComponent<InputField>().textComponent;
        RectTransform txtRT = t.GetComponent<RectTransform>();

        float sizeY = rectTransform.sizeDelta.y / (t.fontSize) * 1.21f;
        rectTransform.sizeDelta = new Vector2(rectTransform.sizeDelta.x, sizeY);

        float posY = txtRT.anchoredPosition.y + (txtRT.sizeDelta.y / 2) - (rectTransform.sizeDelta.y / 2) + rectTransform.sizeDelta.y;
        rectTransform.anchoredPosition = new Vector3(rectTransform.anchoredPosition.x, posY);

        return rectTransform;
    }

    public void ChangeColor(bool correctLine)
    {
        if (followerTransform != null)
        {
            Image img = followerTransform.GetComponent<Image>();

            if (correctLine)
                img.color = correctLineColor;
            else
                img.color = wrongLineColor;
        }
    }

    public void ResetFollower()
    {
        Debug.Log("ResetFollower");
        Destroy(followerTransform.gameObject);
        Destroy(this.gameObject);
    }
}