﻿using UnityEngine.UI;
using UnityEngine;

public class CodeEditorUI : MonoBehaviour
{
    public InputField inputField;
    public Transform tutorialInputField;
    public Text tutorialField;
    public Transform follower;

    private CodeFollower codeFollower;
    private GameController game;

    private void Start()
    {
        game = Managers.instance.game;
    }

    public void GetFollowerReady()
    {
        if (!game.gameSet.levelSettings.tutorial)
            codeFollower = new CodeFollower(inputField.transform, this);
        else
            codeFollower = new CodeFollower(tutorialInputField, this);

        codeFollower.Instantinate();
    }

    public void MoveFollower(bool correctLine)
    {
        codeFollower.MoveFollower(correctLine);
    }

    public void PrepareEditor()
    {
        if (codeFollower != null)
            codeFollower.ResetFollower();

        inputField.text = "";
        if (follower != null)
            Destroy(follower.gameObject);
    }
}