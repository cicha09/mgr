﻿using UnityEngine.UI;
using UnityEngine;
using System.Collections.Generic;
using System.Text.RegularExpressions;

public class CodeEditor : MonoBehaviour
{
    public static CodeEditor instance;
    public CodeEditorUI ui;

    private GameController game;
    public InputField codeField;
    public Text txt;
    public List<string> codeLines = new List<string>();
    public List<CodeBlock> codeBlocks = new List<CodeBlock>();

    public List<KeyValuePair<string, string>> functions = new List<KeyValuePair<string, string>>();

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        game = Managers.instance.game;
        ui = GetComponent<CodeEditorUI>();
    }

    public void CompileCode(bool tutorial)
    {
        if (!tutorial)
        {
            ReadCode();
            ReadCode2();
        }
        else
        {
            game.tutorialController.ui.DisableButtons();
            game.hudController.gameUI.ToggleInteractable(false);
            SetActions(game.tutorialController.ui.GetFunctionsList());
        }
    }

    private void FindBlocks(string allLines)
    {
        string[] lines = allLines.Split('\n');

        codeBlocks = new List<CodeBlock>();

        int lineIndex = 0;

        foreach (var line in lines)
        {
            if (line.Trim() != "" || line.Trim() != " " || line.Trim() != "\n")
            {
                string instruction = game.functionsMatcher.GetInstruction(line.Trim());
                string content = "";
                if (instruction != "")
                {
                    for (int i = lineIndex + 1; i < lines.Length; i++)
                    {
                        content += lines[i] + "\n";

                        if (lines[i].Contains("}"))
                            break;

                    }
                    string condition = Regex.Match(lines[lineIndex], @"\(([^)]*)\)").Groups[1].Value;
                    codeBlocks.Add(new CodeBlock(instruction, condition, content));
                }
                else
                {

                }
            }

            //Regex r_while = new Regex(@"^while(\(|\s)");
            //Match col_while = r_while.Match(line.Trim());

            //if (col_while.Success)
            //{
            //    string content = "";

            //    for (int i = lineIndex + 1; i < lines.Length; i++)
            //    {
            //        content += lines[i] + "\n";

            //        if (lines[i].Contains("}"))
            //            break;

            //    }
            //    string condition = Regex.Match(lines[lineIndex], @"\(([^)]*)\)").Groups[1].Value;
            //    codeBlocks.Add(new CodeBlock("while", condition, content));
            //}
            lineIndex++;
        }
    }

    public List<Code> code;
    private void ReadCode2()
    {
        string[] lines = codeField.text.Split('\n');
        code = new List<Code>();

        for (int i = 0; i < lines.Length; i++)
        {
            string instruction = game.functionsMatcher.GetInstruction(lines[i].Trim());
            if (instruction != "")
            {
                string content = "";
                int l = 0;
                List<KeyValuePair<string, string>> keys = new List<KeyValuePair<string, string>>();
                for (int x = i + 1; x < lines.Length; x++)
                {
                    content += lines[x] + "\n";

                    string fff = game.functionsMatcher.GetFunction(lines[x].Trim());
                    if (fff != "")
                        keys.Add(new KeyValuePair<string, string>(fff, ""));

                    l++;
                    if (lines[x].Contains("}"))
                        break;

                }

                i += l;
                //  string condition = Regex.Match(lines[lineIndex], @"\(([^)]*)\)").Groups[1].Value;
                // codeBlocks.Add(new CodeBlock(instruction, condition, content));

                Code c = new Code();
                c.codeBlock = new CodeBlock(instruction, "true", content);
                c.f = instruction;
                c.codeBlock.functions = keys;
                c.codeBlock.functionsCount = keys.Count;
                code.Add(c);
            }

            string function = game.functionsMatcher.GetFunction(lines[i].Trim());
            if (function != "")
            {
                Code c = new Code();
                c.function = new KeyValuePair<string, string>(function, "");
                c.f = function;
                code.Add(c);
            }
        }
    }

    private void ReadCode()
    {
        string[] lines = codeField.text.Split('\n');
        codeLines = new List<string>(lines);

        functions = new List<KeyValuePair<string, string>>();

        foreach (var line in codeLines)
        {
            if (line.Trim() != "" || line.Trim() != " " || line.Trim() != "\n")
            {
                string function = game.functionsMatcher.GetFunction(line.Trim());
                if (function != "")
                    functions.Add(new KeyValuePair<string, string>(function, "")); // TUTAJ PARAMETR
                else
                    functions.Add(new KeyValuePair<string, string>("", ""));
            }
            else
                functions.Add(new KeyValuePair<string, string>("", ""));
        }

        FindBlocks(codeField.text);
        SetActions(functions);
    }

    private void SetActions(List<KeyValuePair<string, string>> functions)
    {
        ActionsController actionsController = game.actionsController;
        actionsController.MakeActionList(functions);
        actionsController.StartActions();
    }

    public void Writing()
    {
        string[] lines = codeField.text.Split('\n');
        codeLines = new List<string>(lines);
        //FindBlocks(codeField.text);
        txt.text = "";

        foreach (string line in codeLines)
        {
            if (line != "" || line != " " || line != "\n")
            {
                string l = CheckFunction(line);
                if (l != null)
                    txt.text += l;
                else
                    txt.text += line + "\n";
            }
        }
    }

    private string CheckFunction(string line)
    {
        if (line != "")
        {
            Regex r = new Regex(@"^if(\(|\s)");
            Match col = r.Match(line.Trim());

            Regex r_while = new Regex(@"^while(\(|\s)");
            Match col_while = r_while.Match(line.Trim());

            if (line.Trim() == "robot.move();" || line.Trim() == "robot.rotate();" || line.Trim() == "robot.rotateLeft();" || line.Trim() == "robot.rotateRight();")
            {
                return "<b>" + line + "</b>" + "\n";
            }
            else if (col.Success)
            {
                int i = line.Length - (line.IndexOf("if") + 2);

                string s1 = line.Substring(line.IndexOf("if"), 2);
                string s2 = line.Substring(2, i);

                return "<b>" + s1 + "</b>" + s2 + "\n";
            }
            else if (col_while.Success)
            {
                int i = line.Length - (line.IndexOf("while") + 5);

                string s1 = line.Substring(line.IndexOf("while"), 5);
                string s2 = line.Substring(5, i);

                return "<b>" + s1 + "</b>" + s2 + "\n";
            }
            else
                return null;
        }
        else return null;
    }

    public void ReadLines()
    {
        string[] lines = codeField.text.Split('\n');
        codeLines = new List<string>(lines);

        codeField.text = "";

        Regex r = new Regex(@"^if(\(|\s)");
        foreach (string firstLine in codeLines)
        {
            Match col = r.Match(firstLine);

            if (col.Success)
            {
                int i = firstLine.Length - (firstLine.IndexOf("if") + 2);

                string s1 = firstLine.Substring(firstLine.IndexOf("if"), 2);
                string s2 = firstLine.Substring(2, i);

                codeField.text += "<b>" + s1 + "</b>" + s2 + "\n";
            }
            else
                codeField.text += firstLine + "\n";
        }
    }
}

[System.Serializable]
public class CodeBlock
{
    public string construction = "";
    public string condition = "";
    public string content = "";
    public List<KeyValuePair<string, string>> functions;
    public int functionsCount;

    public CodeBlock(string construction, string condition, string content)
    {
        this.construction = construction;
        this.condition = condition;
        this.content = content;
    }
}

[System.Serializable]
public class Code
{
    public CodeBlock codeBlock;
    public string f = "";
    public KeyValuePair<string, string> function;
}