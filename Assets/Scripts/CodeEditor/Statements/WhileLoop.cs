﻿public class WhileLoop 
{
    public string Condition { get; set; }
    public string ExecuteCodes { get; set; }
    public bool Executed { get; set; }
}