﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Linq;
using UnityEngine;

public class FunctionsMatcher : MonoBehaviour
{
    public Dictionary<string, string> Functions { get { return functions; } }

    private Dictionary<string, string> functions;
    private Dictionary<string, string> instructions;
    private Dictionary<string, string> codeLines;

    private void Start()
    {
        functions = new Dictionary<string, string>
        {
            { "move", @"^robot.move\(\)\;$" },
            { "rotate", @"^robot.rotate\(\)\;$" },
            { "moveBy", @"^robot.move\(\d{1,}\)\;$" },
            { "rotateLeft", @"^robot.rotateLeft\(\)\;$" },
            { "rotateRight", @"^robot.rotateRight\(\)\;$"}
        };

        instructions = new Dictionary<string, string>
        {
            { "while", @"^while(\(|\s)" },
            { "if", @"^if(\(|\s)" }
        };

        /* Linie kodu wpisywane w tutorialu. */
        codeLines = new Dictionary<string, string>
        {
            { "move", "robot.move();" },
            { "rotate", "robot.rotate();" },
            { "moveBy", "robot.move(X);" },
            { "rotateLeft", "robot.rotateLeft();" },
            { "rotateRight", "robot.rotateRight();" }
        };
    }

    public string GetFunction(string codeLine)
    {
        var key = "";
        foreach (var pattern in functions.Values)
        {
            if (Match(codeLine, pattern))
            {
                key = functions.FirstOrDefault(x => x.Value == pattern).Key;
                break;
            }
        }

        return key;
    }

    public string GetInstruction(string codeLine)
    {
        var key = "";
        foreach (var pattern in instructions.Values)
        {
            if (Match(codeLine, pattern))
            {
                key = instructions.FirstOrDefault(x => x.Value == pattern).Key;
                break;
            }
        }

        return key;
    }

    public string GetCodeLineByKey(string key)
    {
        return codeLines[key];
    }

    public string GetCodeLineByKey(string key, int parametr)
    {
        return codeLines[key];
    }

    private bool Match(string codeLine, string pattern)
    {
        Regex regex = new Regex(pattern);

        if (regex.Match(codeLine).Success)
            return true;
        else
            return false;
    }
}