﻿using UnityEngine;

public class RobotMovement : MonoBehaviour
{
    public float movingSpeed = 10, rotatingSpeed = 10;

    private GameController game;
    private Animator animator;

    private Vector3 targetPos;
    private Quaternion targetRot;

    private float units = 2;
    private bool moving = false, rotating = false;

    private void Start()
    {
        game = Managers.instance.game;

        if (GetComponent<Animator>())
            animator = GetComponent<Animator>();
    }

    private void Update()
    {
        Moving();
        Rotating();
    }

    private void Moving()
    {
        if (moving)
        {
            transform.position = Vector3.MoveTowards(transform.position, targetPos, Time.deltaTime * movingSpeed);
            if (transform.position == targetPos)
            {
                SetAnimation("Idle");
                moving = false;
            }
        }
    }

    private void Rotating()
    {
        if (rotating)
        {
            transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRot, Time.deltaTime * rotatingSpeed);

            if (transform.rotation == targetRot)
            {
                SetAnimation("Idle");
                rotating = false;
            }
        }
    }

    public void MoveForward()
    {
        Vector3 movement = transform.rotation * Vector3.forward * units;
        targetPos = transform.position + movement;

        SetAnimation("Walk");
        moving = true;
    }

    public void MoveForward(int steps)
    {
        Vector3 movement = transform.rotation * Vector3.forward * units * steps;
        targetPos = transform.position + movement;

        SetAnimation("Walk");
        moving = true;
    }

    //public void RotateRight()
    //{
    //    targetRot = transform.rotation;
    //    targetRot.eulerAngles = new Vector3(targetRot.eulerAngles.x, targetRot.eulerAngles.y + 90, targetRot.eulerAngles.z);

    //    SetAnimation("Turn");
    //    rotating = true;
    //}

    public void Rotate(int rot)
    {
        targetRot = transform.rotation;
        targetRot.eulerAngles = new Vector3(targetRot.eulerAngles.x, targetRot.eulerAngles.y + rot, targetRot.eulerAngles.z);

        SetAnimation("Turn");
        rotating = true;
    }

    private void SetAnimation (string triggerName)
    {
        if (animator != null)
            animator.SetTrigger(triggerName);
    }

    public bool ReachedDestinationTile()
    {
        RaycastHit hit;
        bool reachedTile = false;
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.down), out hit, 2))
            reachedTile = game.boardController.CheckDestinationTileReached(hit);
        return reachedTile;
}

    public void StopTransforming()
    {
        rotating = false;
        moving = false;
    }
}
