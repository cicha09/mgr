﻿using UnityEngine;
using System.Collections.Generic;
using UnityEditor;
using UnityEditorInternal;
using System.Linq;

[CustomEditor(typeof(Strings))]
public class StringsEditor : Editor
{
    private ReorderableList list;

    public void OnEnable()
    {
        list = new ReorderableList(serializedObject, serializedObject.FindProperty("entries"), true, true, true, true);
        list.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) =>
        {
            SerializedProperty element = list.serializedProperty.GetArrayElementAtIndex(index);
            rect.y += 2;
            EditorGUI.PropertyField(new Rect(rect.x, rect.y, 30, EditorGUIUtility.singleLineHeight), element.FindPropertyRelative("string_id"), GUIContent.none);
            EditorGUI.PropertyField(new Rect(rect.x + 40, rect.y, 160, EditorGUIUtility.singleLineHeight), element.FindPropertyRelative("string_EN"), GUIContent.none);
            EditorGUI.PropertyField(new Rect(rect.x + 210, rect.y, 160, EditorGUIUtility.singleLineHeight), element.FindPropertyRelative("string_PL"), GUIContent.none);
        };

        list.onAddCallback = (ReorderableList list) =>
        {
            int index = list.serializedProperty.arraySize;
            list.serializedProperty.arraySize++;
            list.index = index;

            SerializedProperty element = list.serializedProperty.GetArrayElementAtIndex(index);
            element.FindPropertyRelative("string_id").intValue = index;
            element.FindPropertyRelative("string_EN").stringValue = "EN " + index.ToString();
            element.FindPropertyRelative("string_PL").stringValue = "PL " + index.ToString();
        };
    }

    public override void OnInspectorGUI()
    {
        Strings myTarget = (Strings)target;

        serializedObject.Update();
        list.DoLayoutList();
        serializedObject.ApplyModifiedProperties();

        if (GUILayout.Button("WRITE TO EXCEL"))
        {
            ExcelData excelData = new ExcelData();
            excelData.Write(myTarget.entries);
        }
        if (GUILayout.Button("READ FROM EXCEL"))
        {
            ExcelData excelData = new ExcelData();
            myTarget.entries = excelData.Read();
        }
        if (GUILayout.Button("MAKE BACKUP"))
        {
            ExcelData excelData = new ExcelData();
            excelData.MakeBackup(myTarget.entries);
        }
        if (GUILayout.Button("UPDATE LOCALIZED TEXTS ON SCENE: " + myTarget.gameSettings.CurrentLanguage))
        {
            ChangeTextOfAllString(myTarget);
            InternalEditorUtility.RepaintAllViews();
        }
    }

    public void ChangeTextOfAllString(Strings myTarget)
    {
        List<LocalizedText> localizedTexts = FindObjectsOfTypeAll<LocalizedText>();
        foreach (var localizedText in localizedTexts)
        {
            UnityEngine.UI.Text txt = localizedText.GetComponent<UnityEngine.UI.Text>();
            txt.text = myTarget.GetValueByKey(localizedText.key);
        }
    }

    private static List<T> FindObjectsOfTypeAll<T>()
    {
        List<T> results = new List<T>();
        for (int i = 0; i < UnityEngine.SceneManagement.SceneManager.sceneCount; i++)
        {
            var s = UnityEngine.SceneManagement.SceneManager.GetSceneAt(i);
            if (s.isLoaded)
            {
                var allGameObjects = s.GetRootGameObjects();
                for (int j = 0; j < allGameObjects.Length; j++)
                {
                    var go = allGameObjects[j];
                    results.AddRange(go.GetComponentsInChildren<T>(true));
                }
            }
        }
        return results;
    }
}
