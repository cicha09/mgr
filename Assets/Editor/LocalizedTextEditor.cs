﻿using UnityEditor;
using System.Linq;

[CustomEditor(typeof(LocalizedText))]
public class LocalizedTextEditor : Editor
{
    Strings strings;

    private void OnEnable()
    {
        strings = UnityEngine.Resources.Load("Data/Strings", typeof(Strings)) as Strings;
    }

    public override void OnInspectorGUI()
    {
        LocalizedText myTarget = (LocalizedText)target;

        serializedObject.Update();

        EditorGUILayout.BeginVertical("Box");
        myTarget.key = EditorGUILayout.IntField("String Key", myTarget.key);

        bool exists = strings.entries.Any(u => u.string_id == myTarget.key);
        if (exists)
        {
            var entry = strings.entries.Where(s => s.string_id == myTarget.key).Single();
            EditorGUILayout.LabelField(entry.string_EN +  " | " + entry.string_PL, EditorStyles.miniLabel);
        } else
            EditorGUILayout.LabelField("It doesn't exist! Key: " + myTarget.key, EditorStyles.miniLabel);

        EditorGUILayout.EndVertical();
        serializedObject.ApplyModifiedProperties();
    }
}
 